#!/usr/bin/env bash
#======================================================================#
#								       #
# This script will prepare a batch file to be run within IGV	       #
#								       #
#======================================================================#

#Variable=definitions==================================================#
Range=20 # the screenshot will be extended to Range position on either
	 # right and left. Change the value if needed
#======================================================================#
gethelp(){
cat  <<EOF
This script create a batch file for IGV, creating a serie of screenshot at particular location
3 arguments are expected :

 o The name of a tsv file , with 3 fields : chromosome number in the
 form chr1 as instance and position start and end (in bp) on the genome

 o The name of a igv session. Take care to prepare it properly (as it
will be the way the screenshot will looks like)

 o The directory name where screenshots should be stored
===============================================================
NB :

You should make sure that the file and path to it are correctly
configured.
Something like
C:\\my~Documents\\IGV\\ScreenshotDir\\ on Windows
or
/home/me/IGV/ScreenshotDir/ on Unix/BSD
EOF
}

#First test if 3 arguments have been given 
#if [ "$#" -ne "3" ] && [ $1 -ne "-h" ]; then
if [[ $1 == "-h" ]];then #If help is asked
    gethelp
elif [ "$#" -ne "3" ]; then
    clear
    echo "!!!!!!!!!!!!!!!!!"
    echo "Too few arguments"
    echo "3 arguments expected :"
    echo "#1 : A tsv file with position to test "    
    echo "#2 : A igv session file (xml)"
    echo "#3 : A directory to store screenshots \n"
    echo "=example========================================== \n"
    echo "./PrepIgvBatch [fullpath]/PosToTest.csv [fullpath]/IgvSession.xml [fullpath]/ScreenShotDir"
    echo "where [fullpath] will be the absolute path for the file"
    echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    exit ;
fi

gethelp(){
cat  <<EOF
This script create a batch file for IGV, creating a serie of screenshot at particular location 
3 arguments are expected :

 o The name of a tsv file , with 3 fields : chromosome number in the
 form chr1 as instance and position start and end (in bp) on the genome

 o The name of a igv session. Take care to prepare it properly (as it
will be the way the screenshot will looks like)

 o The directory name where screenshots should be stored
=============================================================== 
NB :

You should make sure that the file and path to it are correctly
configured. 
Something like 
C:\\my~Documents\\IGV\\ScreenshotDir\\ on Windows
or 
/home/me/IGV/ScreenshotDir/ on Unix/BSD
EOF
}


#========================================================
PosFile=$1;  
SesFile=$2; 
ImgDir=$3; 
OutFile="igv.batch"; 
date=`date "+%d_%m_%Y"`

if [ ! `dirname $SesFile` ] ||  [ ! `dirname $PosFile` ] ||  [ ! `dirname $ImgDir` ]; then
    echo "You must provide FULL PATH files"; exit
fi  
echo "........................................................."
if [ ! -f $PosFile ] ; then
    echo "The position file doesn't exist"
else
    echo  "FILE POSITION=$PosFile";
fi

if [ ! -d "$ImgDir" ]; then
    echo "The directory $ImgDir";
    echo "does not exist."
    echo "creating...."
    mkdir -p $ImgDir || die
fi
    echo "OUTPUT DIR=$ImgDir";

if [ -f ${ImgDir}/${OutFile} ]; then 
   echo "file $OutFile exist!"
   OutFile=${OutFile}_${date}
fi
   echo "OUTPUT FILE=${ImgDir}/${OutFile}";


#========================================================
cat > ${ImgDir}/$OutFile <<OUT
new
load ${SesFile}
snapshotDirectory ${ImgDir}

OUT
echo $PosFile
echo "while loop"
#cat $PosFile
cat $PosFile| awk -F"\t" /chr[1-9XY]+:/ | awk -F"\t" '{OFS="\t";print $2}'| sed 's/:/-/'| awk -F"-" '{OFS="\t";print $1,$2,$3}'|sort -k1,1 -k2,2n -u  | while read myline 
   do
   #echo "MYLINE" $myline 
   #echo $myline| awk -v range=${Range} -F"\t" '{OFS="\t";print "goto " $1":"($2-range)"-"($2+range)" \nsnapshot"}' - >> ${ImgDir}/$OutFile
   echo $myline| awk -v range=${Range} '{OFS="\t";print "goto " $1":"($2-range)"-"($3+range)" \nsnapshot"}' - >> ${ImgDir}/$OutFile
done
echo "while end"

#cat >> ${ImgDir}/$OutFile <<OUT
#exit
#OUT



cat <<EOF
....................................................................
The script finished without problems.  Batch file to be processed is
"igv.batch" it should be uploaded within IGV (via the file/ run batch
script menu). 

If you access data through a das or protected server, you should
connect to the server before running the batch script.

If you use windows check path are well defined (and correct them if
needed), before running the batch script.
....................................................................
EOF

